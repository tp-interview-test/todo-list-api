const express = require("express");
const { v4: uuidv4 } = require("uuid");

const router = express.Router();

const todoList = [];

router.get("/", (req, res) => {
  return res.json(todoList);
});

router.post("/", (req, res) => {
  if (!req.body || !req.body.text) {
    return res.status(400).json({
      success: false,
      message: "Cannot empty!",
    });
  }

  todoList.push({
    id: uuidv4(),
    text: req.body.text,
  });

  return res.json({
    success: true,
    message: "Add todo successful.",
  });
});

router.put("/:id", (req, res) => {
  if (!req.body || !req.body.text) {
    return res.status(400).json({
      success: false,
      message: "Cannot empty!",
    });
  }

  const item = todoList.find(({ id }) => id == req.params.id);

  if (!item) {
    return res.status(404).json({
      success: false,
      message: "Data not found!",
    });
  }

  item.text = req.body.text;

  return res.json({
    success: true,
    message: "Update todo successful.",
  });
});

router.delete("/:id", (req, res) => {
  const itemIndex = todoList.findIndex(({ id }) => id == req.params.id);

  if (itemIndex == -1) {
    return res.status(404).json({
      success: false,
      message: "Data not found!",
    });
  }

  todoList.splice(itemIndex, 1);

  return res.json({
    success: true,
    message: "Delete todo successful.",
  });
});

module.exports = router;
