const express = require("express");
const todos_route = require("./routes/todos.route");

const app = express();
const port = 3000;

app.use(express.json());

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.use("/todos", todos_route);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
